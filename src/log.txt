2024-10-21T23:00:00+02:00	performed a small, screen-based update to jarotsim, for it to run better in smaller and bigger screens	{jarotsim}
2024-09-30T23:59:00+02:00	submitted a proposal for a workshop at ICLC 2025, qiudanz technique: computational manipulation of minimalist movement sequences	{proposal iclc 2025 workshop}
2024-09-28T11:00:00+02:00	facilitated a qiudanz technique workshop in a merveilles.town meetup	{qiudanz devlog}
2024-05-25T15:30:00+02:00	gran día compartiendo nuestro taller de compudanzas en el congreso esLibre 2024 en Valencia	{taller esLibre24}
2024-04-25T21:00:00+02:00	compartimos nuestra charla de compudanzas: explorando computación sin prisas, en bailes, juegos y papel en la Facultad de Ciencias UNAM	{talks and workshops}
2024-04-18T22:22:00+02:00	we dedicate this work to the public domain	{public domain}
2024-04-18T22:00:00+02:00	dedicamos este trabajo al dominio público	{dominio público}
2024-04-17T20:00:00+02:00	preparing an upcoming online talk (april 25th) and offline workshop (may 25th)!	{talks and workshops}
2024-03-09T12:00:00+01:00	started to stream our maintenance practice: a long overdue revision and update of the uxn tutorial	{maintenance practice}
2024-01-28T22:00:00+01:00	enviamos una propuesta de taller al congreso esLibre 2024: computación somática: materializando ciencias computacionales en cuerpxs humanxs y coreografía	{propuesta taller computación somática}
2024-01-06T20:00:00+01:00	we found, edited and published some notes and prototypes related to POÑG, from 12020	{poñg}
2023-12-27T16:00:00+01:00	publicamos nuestra charla relámpago sobre jarotsim, com subtítulos en español e inglés	{jarotsim en hlci}
2023-12-27T16:00:00+01:00	uploaded our lightning talk about jarotsim, with captions in english and spanish	{jarotsim at hlci}
2023-12-11T16:30:00+01:00	we presented jarotsim: a playground for the discovery, exploration and livecoding of Turing machines, at the Hybrid Live Coding Interfaces 2023: Boundless Thinking workshop. we had a lot of fun!	{jarotsim at hlci}
2023-11-26T17:00:00+01:00	updates en la identidad visual: dark/light themes y nuevos colores	{identidad visual}
2023-10-22T13:00:00+02:00	migramos servidor y la sala ssb queda offline	{la sala}
2023-10-21T17:00:00+02:00	publicada la traducción al español de norpet	{norpet mascota digital}
2022-12-06T19:00:00+01:00	performed qiudanz tag on the Hybrid Live Coding Interfaces Workshop 2022	{qiudanz tag}
2022-12-04T11:00:00+01:00	wrote guiding notes for the qiudanz tag exploration that we will be showcasing on the Hybrid Live Coding Interfaces 2022 workshop.	{qiudanz tag}
2022-11-13T19:00:00+01:00	new version (1.0.4) of introduction to uxn programming e-book: launcher and raw runes	{introduction to uxn programming book}
2022-10-31T12:00:00+01:00	accepted in the Hybrid Live Coding Interfaces 2022 workshop with our qiudanz technique proposal	{qiudanz devlog}
2022-10-03T19:00:00+02:00	applied to the Hybrid Live Coding Interfaces 2022 workshop	{qiudanz devlog}
2022-06-28T07:00:00+02:00	moved from cdmx to madrid	{location}
2022-04-22T14:00:00-05:00	new version of our introduction to uxn e-book with the screen auto byte and other updates in varvara devices	{introduction to uxn programming book}
2022-04-07T20:00:00-05:00	released jarotsim, a playground for the discovery, exploration and livecoding of turing machines	{jarotsim}
2022-04-02T12:00:00-06:00	started developing jarotsim, a playful tool to explore and livecode turing machines	{jarotsim}
2022-03-27T19:00:00-06:00	started using esglua, our static site generator	{esglua}
2022-03-20T12:00:00-06:00	performed (a)live computing dance on algorave 10th birthday	{alive computing dance}
2022-02-09T19:00:00-06:00	updated our introduction to uxn e-books with a new table formatting and some minor corrections	{publications}
2022-02-01T11:00:00-06:00	created la sala, experimental, slow, and online-but-offline-first community based on ssb	{la sala}
2022-01-23T17:00:00-06:00	submitted norpet, a digital pet with hand-powered logic, to the virtual pet jam	{norpet}
2022-01-15T19:00:00-06:00	publicamos el ebook: introducción a programación uxn!	{introducción a programación uxn}
2022-01-09T12:00:00-06:00	published our ebook: introduction to uxn programming!	{introduction to uxn programming book}
2022-01-06T16:00:00-06:00	notas para colaborar con traducciones	{traducciones}
2021-12-17T14:00:00-06:00	built the tomatimer, a pomodoro timer on an attiny85 programmed with avr-asm	{tomatimer}
2021-12-03T16:00:00-06:00	qiudanz technique in the slomoco fall gathering	{qiudanz devlog}
2021-12-01T00:30:00-06:00	started advent of code 2021 using awk	{advent of code 2021}
2021-11-26T16:00:00-06:00	fourth qiudanz technique mini workshop	{qiudanz devlog}
2021-11-23T20:00:00-06:00	third qiudanz technique mini workshop	{qiudanz devlog}
2021-11-21T17:00:00-06:00	taught and enjoyed our intro to uxn programming online workshop via babycastles academy!	{intro to uxn programming}
2021-11-19T15:00:00-06:00	first check-in meeting with slomoco fall microresidents	{qiudanz devlog}
2021-11-17T17:00:00-06:00	created a short introductory video to qiudanz mode 1	{qiudanz mode 1}
2021-11-15T20:00:00-06:00	second qiudanz technique mini workshop	{qiudanz devlog}
2021-11-12T16:00:00-06:00	first qiudanz technique mini workshop	{qiudanz devlog}
2021-11-04T13:00:00-06:00	announcing our intro to uxn programming online workshop, via babycastles academy! sunday, nov 21	{intro to uxn programming}
2021-11-03T14:00:00-06:00	preliminary notes for a workshop on qiudanz technique mode 1	{qiudanz mode 1}
2021-10-24T17:00:00-05:00	created an account for the fediverse: compudanzas@post.lurk.org	{contact}
2021-10-19T17:00:00-05:00	got accepted to develop the qiudanz technique in the slomoco fall phase!	{qiudanz technique}
2021-10-12T21:00:00-05:00	published uxn tutorial day 7, more devices	{uxn tutorial day 7}
2021-09-14T21:00:00-05:00	applied to the fall phase of slomoco	{slomoco application}
2021-09-14T18:00:00-05:00	published uxn tutorial day 6, towards pong	{uxn tutorial day 6}
2021-08-24T20:00:00-05:00	published uxn tutorial day 5, the mouse and uxntal goodies	{uxn tutorial day 5}
2021-08-18T15:00:00-05:00	updated the uxn tutorial with changes in the instructions and opcodes NIP, INC, and LIT	{uxn tutorial}
2021-08-11T20:00:00-05:00	published uxn tutorial day 4, variables and animation loop!	{uxn tutorial day 4}
2021-07-31T17:00:00-05:00	published uxn tutorial day 3	{uxn tutorial day 3}
2021-07-27T21:00:00-05:00	published uxn tutorial day 2	{uxn tutorial day 2}
2021-07-22T21:00:00-05:00	published day 1 of the uxn tutorial!	{uxn tutorial}
2021-07-15T12:00:00-05:00	reading: starting forth. possible practice, puzzle, game, eventually dance?	{forth}
2021-07-12T12:00:00-05:00	published the uxnería repo for uxn projects and sketches. excited about the nibble dice tracker	{nibble dice tracker}
2021-07-05T21:00:00-05:00	fourth session of the compudanzas workshop: we shared our máquina universal bailable	{mub}
2021-06-30T21:00:00-05:00	third session of the compudanzas workshop: we learned and performed the danzasistemas-tag (first time ever!) and a turing machine in d-turing mode	{las danzas}
2021-06-28T21:00:00-05:00	second session of the compudanzas workshop: we performed reglas de wolfram and talked a lot about emergent complexity	{reglas de wolfram}
2021-06-23T21:00:00-05:00	first session of the online compudanzas workshop! we danced the ciclo de memoria and danzas compuertas	{las danzas}
2021-06-21T17:00:00-05:00	created the compudanzas page on patreon: support us and help us achieve the goal of having this dream project as our main activity!	{support}
2021-06-19T20:00:00-05:00	pcd quito 2021: we had a talk on compudanzas and a workshop on coloring computers.	{talks and workshops}
2021-06-17T22:00:00-05:00	compudanzas.net is now available in gemini: sharing (self-hosted) space with caracolito.mooo.com :)	{gemini}
2021-06-16T18:00:00-05:00	digging through the projects archives	{proposals}
