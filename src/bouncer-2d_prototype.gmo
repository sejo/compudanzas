# bouncer2d-8x6 tabletop computer (prototype)
lang=en es->{bouncer-2d prototipo}
a tabletop non-electronic, human-scale, very-slow computer with an 8x6 monochromatic screen. a prototype of {beans computing}.

following a simple set of rules, you can collaborate with it to compute and display an animation of a ball bouncing in the walls of the screen, one frame at a time.

=> ./img/secuencia_bounce2d-8x6-0a3.gif a sequence of four frames showing two paper boards with black and soy beans in them: the one in the left contains the processing cells, and the one in the right looks like a screen composed of soy beans except for one, which seems to be moving from the top left corner to the bottom and right.

# downloads

=> https://ipfs.io/ipfs/QmfQwdoc68dPXnGTspTiWu8xjpCLGUaH7YYFUmx84YUtwS/ bouncer2d-8x6 ready to be printed (pdf)

# structure 

the computer has two pages, both of them with "cells". it has a total of four sections.

first page:

* top row: state of the machine (encoded)
* middle block: process cells
* bottom row: next state of the machine (encoded)

second page:

* 8x6 monochromatic screen

# how to use:

* first of all, choose two classes of elements, A and B (e.g. A="black beans" and B="chickpeas").
* each cell can only have one of those elements at a given time.

## for the top row

* if this is the first iteration, fill it with an arbitrary combination of your elements (e.g. all the cells with a chickpea in each)
* otherwise, when you finish an iteration and you are ready to start a new one, transfer the elements from the bottom row to the top row, keeping the same order.

## for all the other cells

* observe the cells in the computer: each one has a name at the top, and a list of 1 to 3 cell names that correspond to its inputs.
* for each cell, remember and apply the rule: put an element of class A inside the cell only when all of its inputs are B. otherwise put inside an element of class B.
* example rule: put a black bean inside the cell only when all of its inputs are chickpeas. otherwise, put a chickpea inside.
* when you finish iterating through all the cells, you would have completed a frame of the animation in the screen. capture it somehow!
