# ICLC 2025 proposal — qiudanz technique: computational manipulation of minimalist movement sequences

a proposal for a {qiudanz technique} workshop at the International Conference on Live Coding (ICLC) 2025; qiudanz technique: computational manipulation of minimalist movement sequences
=> https://iclc.toplap.org/2025/ ICLC 2025

submitted on 12024-09-30.

this proposal was rejected because, paraphrasing, it wasn't deemed as a good fit for the "workshop format and expectations" (?)

# General information

* Workshop Title: qiudanz technique: computational manipulation of minimalist movement sequences
* Workshop Duration: short (2h)
* Target Audience: General public
* Workshop Language: English but we also speak Spanish

# Abstract

The {qiudanz technique} is a movement practice based on the computational transformation of movement sequences. Its purpose is to create and share dances based on abstract computational machines. The technique is part of a commitment to playfully disseminate and preserve computer science beyond electronic devices and industrial civilization. 

The workshop consists of a guided movement and play session in which we will (re)connect with our bodies and re-appropriate computational concepts for the sake of creating nerdy dances and having a fun time. In the workshop, we will introduce and explore the minimalist movement vocabulary of the technique, learn how these movements are combined into sequences, and practice queue-based computational operations to transform these sequences while dancing them. We will engage in several one-to-many and one-to-one activities and games based on the technique. The experience will combine cognitive abilities such as memory and logic thinking with movement coordination and somatic expression.

To participate in the workshop, we welcome people concerned about the environmental and social impacts of digital technologies, as well as people curious about embodying computational thinking. Previous experience with movement practices or computational thinking is not needed. All the activities are designed to be adapted to different ranges of mobility. The workshop engages with human-scale computing and provides a possible answer to the question: what would happen if computers were dances instead of closed electronic devices?


# Workshop Material

=> https://spectra.video/w/dtoM76wLmuQfabP8t8ou43 video: intro to qiudanz technique mode 1
=> ./qiudanz_technique.gmi {qiudanz technique}
=> ./taller_eslibre24.gmi {taller esLibre24}

# Program Note

The {qiudanz technique} is a movement practice based on the computational transformation of movement sequences. Its purpose is to create and share dances based on abstract computational machines. The technique is part of a commitment to playfully disseminate and preserve computer science beyond electronic devices and industrial civilization. 

The workshop consists of a guided movement and play session in which we will (re)connect with our bodies and re-appropriate computational concepts for the sake of creating nerdy dances and having a fun time. In the workshop, we will introduce and explore the minimalist movement vocabulary of the technique, learn how these movements are combined into sequences, and practice queue-based computational operations to transform these sequences while dancing them. The workshop engages with human-scale computing and provides a possible answer to the question: what would happen if computers were dances instead of closed electronic devices?


# ICLC 2025 -- Technical Requirements

Workshops

## 1. General Information

### 1.1 How many people is the workshop intended for, and at what level of expertise?

30 people, general public. No previous experience in movement practices is needed.

### 1.2 Do the participants need to bring anything?

No need to bring a laptop. Comfortable clothing could be helpful.

## 2. Materials Needed

### 2.1 What type of sound system do you need?

A full PA system would be great.

### 2.2 How many video projectors does your workshop need?

No need for projectors.

### 2.3 How much table space will each participant need?

No need for tables. We would need enough open space for 30 people to be able to move comfortably.

### 2.4 Anything else you need?

Microphones for two instructors and one percussionist.
