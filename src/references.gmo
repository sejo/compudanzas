# references

a list of external resources that inspire and/or inform our work.

see also our {bookmarks} for friends' sites and capsules.

# permacomputing / computing within limits

references related to {permacomputing}

=> https://permacomputing.net/ Permacomputing wiki


## from viznut
=> http://viznut.fi/texts-en/permacomputing.html permacomputing | viznut
=> http://viznut.fi/texts-en/permacomputing_update_2021.html permacomputing update 2021 | viznut
=> http://viznut.fi/texts-en/digital_esthetics.html Digital esthetics, environmental change and the subcultures of computer art | viznut

## more concepts 

=> https://computingwithinlimits.org/ LIMITS Workshop on Computing Within Limits
=> https://wimvanderbauwhede.github.io/articles/frugal-computing/ Frugal Computing - Wim Vanderbauwhede
=> https://computingwithinlimits.org/2021/papers/limits21-devalk.pdf Marloes de Valk: A pluriverse of local worlds: a review of Computing within Limits related terminology and practices
=> http://go-dh.github.io/mincomp/about/ Minimal computing
=> https://dukespace.lib.duke.edu/dspace/handle/10161/14457 Digital Environmental Metabolisms: An Ecocritical Project of the Digital Environmental Humanities 

## from xxiivv

=> https://wiki.xxiivv.com/site/collapse_computing.html XXIIVV - collapse informatics
=> https://wiki.xxiivv.com/site/permacomputing.html XXIIVV - permacomputing
=> https://wiki.xxiivv.com/site/longtermism.html XXIIVV - longtermism

## from low tech magazine

=> https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html How and why I stopped buying new laptops - low tech magazine
=> https://solar.lowtechmagazine.com/es/2018/10/bedazzled-by-energy-efficiency.html Deslumbrados por la Eficiencia Energética - low tech magazine
=> https://solar.lowtechmagazine.com/2009/06/embodied-energy-of-digital-technology.html The monster footprint of digital technology - low tech magazine

## in gemini

texts by solderpunk in the {gemini} protocol

=> gemini://zaibatsu.circumlunar.space/~solderpunk/phlog/discussions-toward-radically-sustainable-computing.txt Discussions toward radically sustainable computing - solderpunk
=> gemini://zaibatsu.circumlunar.space/~solderpunk/phlog/the-standard-salvaged-computing-platform.txt the standard salvage computing platform - solderpunk

## art projects

=> https://www.youtube.com/watch?v=YMKJ7S7fKOk Rare Earthenware - Unknown Fields
=> https://web.archive.org/web/20210415134117/http://www.unknownfieldsdivision.com/summer2014china-aworldadriftpart02.html Unknown Fields Division - Summer 2014 China Cargo ship expedition

## related discussion
=> https://emreed.net/hand-held.html The Handheld Is Dead! Long Live The Handheld! - Emilie M. Reed

# non-electronic computers
			
=> https://yip.pe/analog.html analogue computing

## human-powered
=> https://110010100.neocities.org/computacion-humana 110010100 - Computación Humana
=> https://wintermute.org/project/Rustic_Computing/ Brendan Howell - Rustic Computers
=> https://www.jeffreythompson.org/human-computers.php Jeff Thompson ++ Human Computers
=> https://xkcd.com/505  a bunch of rocks, randall munroe, xkcd
=> https://www.youtube.com/watch?v=rA5qnZUXcqo The Stilwell Brain - Vsauce (youtube)

### paper
=> https://wiki.xxiivv.com/site/papier.html wdr papier computer
=> https://wiki.xxiivv.com/site/paper_computing.html paper computing


## marbles

=> https://www.turingtumble.com/ turing tumble
=> https://www.youtube.com/watch?v=QKnSRw_X2w4 Rule 110 marble computer (youtube)

## fluidics

=> ./fluidics.gmi {fluidics}
=> http://www.blikstein.com/paulo/projects/project_water.html programmable water - paulo blikstein 2003	

## dominoes
=> https://www.gwern.net/docs/www/think-maths.co.uk/5d2586e9724de4c5df5b354ee6117fcc2e1b722b.pdf Logic Gates - Domino Computer
=> https://www.omanobserver.om/article/44559/Business/aba-oman-students-set-world-record-for-dominoes-circuit ABA Oman students set world record for dominoes circuit

## lambda

=> https://www.media.mit.edu/projects/2d-an-exploration-of-drawing-as-programming-language-featuring-ideas-from-lambda-calculus/overview/ λ-2D: An Exploration of Drawing as Programming Language, Featuring Ideas from Lambda Calculus
=> https://tromp.github.io/cl/diagrams.html lambda diagrams

## misc
=> https://esoteric.codes/blog/escher-circuits-using-vision-to-perform-computation Escher Circuits: Using Vision to Perform Computation
=> https://groups.csail.mit.edu/mac/users/bob/sliding-blocks.pdf The Complexity of Sliding-Block Puzzles and Plank Puzzles - Robert A. Hearn

# dance and computers

=> https://www.analivia.com.br/computer-dance-3/ Computer Dance - Analivia Cordeiro

# solarpunk

=> https://www.youtube.com/watch?v=hHI61GHNGJM What is solarpunk? -  Andrewism (yt)
=> https://www.re-des.org/un-manifiesto-solarpunk/ Un Manifiesto Solarpunk - ReDes
=> https://anzacgf.home.blog/2019/07/12/an-anarchist-solution-to-global-warming-peter-gelderloos/ An Anarchist Solution to Global Warming ~ Peter Gelderloos

# computer science

=> https://archive.org/details/computationfinit0000mins/ Computation: finite and infinite machines (1967) Minsky
=> https://dspace.mit.edu/bitstream/1721.1/6107/2/AIM-052.pdf Universality of Tag Systems with P = 2 (1964) Cocke and Minsky
=> https://wpmedia.wolfram.com/uploads/sites/13/2018/02/15-1-1.pdf Cook, Matthew (2004). "Universality in Elementary Cellular Automata" (PDF). Complex Systems. 15: 1–40.
=> https://www.youtube.com/watch?v=xP5-iIeKXE8 A video of Conway's Game of Life, emulated in Conway's Game of Life.
=> https://aphyr.com/posts/341-hexing-the-technical-interview Hexing the technical interview
=> http://www.martin-gardner.org/SciAm1.html#1 Martin Gardner and Scientific American
=> https://grahamshawcross.com/2012/10/12/wang-tiles-and-turing-machines/ Wang Tiles and Turing Machines

## studies

=> http://socialstudies.cs.mcgill.ca/index.html Social Studies of Computing Research Group
=> https://queersts.com/work-group/manifest/ Queer STS (Science and Technology Studies) Manifest

## preservation

=> http://www.vpri.org/pdf/tr2015004_cuneiform.pdf The Cuneiform Tablets of 2015 Long Tien Nguyen, Alan Kay
=> https://retrocomputingforum.com/t/virtual-machines-and-one-page-computing/1477/ Virtual Machines and One Page Computing (discussion)
=> https://web.archive.org/web/20010625015518/http://www.nla.gov.au/padi/topics/19.html Emulation


# emergent complexity

=> https://writings.stephenwolfram.com/2020/04/finally-we-may-have-a-path-to-the-fundamental-theory-of-physics-and-its-beautiful/ Finally We May Have a Path to the Fundamental Theory of Physics… and It’s Beautiful - Stephen Wolfram Writings

# collapsology

perspectives on living under {collapse} awareness

=> https://jembendell.com/2019/05/15/deep-adaptation-versions/ versions of the deep adaptation paper - jem bendell
=> https://www.opendemocracy.net/en/oureconomy/deep-adaptation-opens-necessary-conversation-about-breakdown-civilisation/ Deep Adaptation opens up a necessary conversation about the breakdown of civilisation
=> http://birdsbeforethestorm.net/2019/12/how-to-live-like-the-world-is-ending/ how to live like the world is ending - margaret killjoy
=> https://theanarchistlibrary.org/library/margaret-killjoy-take-what-you-need-and-compost-the-rest-an-introduction-to-post-civilized-theo take what you need and compost the rest: an introduction to post-civilized theory - margaret killjoy
=> https://gendread.substack.com/p/it-is-time-to-step-into-the-role it is time to step into the role of the "prospective survivor" - britt wray
=> https://www.resilience.org/stories/2019-05-10/loving-a-vanishing-world/ loving a vanishing world - emily johnston
=> http://gaianism.org/living-with-collapse/ Living with Collapse By Erik Assadourian

## possibilities?

=> https://deepgreenresistance.org/ deep green resistance
=> https://stopfossilfuels.org/ stop fossil fuels
solarpunk, see above
