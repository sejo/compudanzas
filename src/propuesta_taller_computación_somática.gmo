# propuesta de taller para esLibre 2024
lang=es
enviamos la siguiente propuesta para participar en el congreso esLibre 2024 con el taller computación somática: materializando ciencias computacionales en cuerpxs humanxs y coreografía

=> https://eslib.re/2024/ Congreso esLibre 2024
=> https://propuestas.eslib.re/2024/talleres/ Talleres esLibre 2024

enviada el 12024-01-28.

la propuesta fue aceptada, ver {taller esLibre24}.

# título

computación somática: materializando ciencias computacionales en cuerpxs humanxs y coreografía

# resumen

¿qué es una computadora? ¿cómo es tan poderosa y hace lo que hace, si consiste en piezas de minerales ordenados?
¿qué pasa si la alentamos? ¿y si incrementamos su tamaño? ¿y si le quitamos todo rastro de eficiencia para que la podamos bailar?

en este taller nos insertaremos entre el mundo abstracto de las ciencias computacionales y el mundo de su materialización industrial, para darle cuerpx humanx, social, coreográfico, a algunos de sus conceptos fundamentales.

lo que queremos es construir y programar computadoras relativamente lentas, grandes e ineficientes, a partir de personas en movimiento siguiendo y jugando instrucciones. así, nos podremos reapropiar de las maneras computacionales de entender el mundo, para usarlas a nuestro gusto con el fin de proponer otras posibles computadoras, danzas, y/o formas de vida.


# descripción

el taller consiste en una sesión guiada de movimiento y juego en la que pasaremos por varias etapas para conectar con nuestrxs cuerpxs y visibilizar procesos e ideas que rigen las realidades digitales actuales. el propósito es instigar e investigar la reapropriación de dichos conceptos computacionales con el objetivo de imaginar y realizar mundos donde estos sirven a intereses alternos, como los de la danza, la vida y/o la liberación. 

dado lo tradicionalmente abstracto y “cerebral” de las ciencias computacionales, ponemos mucho énfasis en (re)visitarlas como cuerpxs multidimensionales que somos.

las etapas del taller son: sensibilización inicial y calentamiento, movimiento discreto, introducción a la técnica qiudanz ({qiudanz technique}) de manipulación computacional de secuencias de movimiento, exploración de conceptos computacionales a través de actividades y juegos basados en la técnica, y un espacio para probar ideas de quienes participen.

los conceptos computacionales a cuerpear: estados finitos y tiempos discontinuos, operaciones lógicas, memoria digital, datos e instrucciones, y algunas máquinas computacionales abstractas.

la duración del taller se puede adaptar entre 90 y 120 minutos, de acuerdo a las necesidades del comité organizador.
# público objetivo

dadas las crisis climáticas, ecológicas y sociales actuales, consideramos que el taller es un importante ejercicio de imaginación donde nos preguntamos: ¿qué pasaría si las computadoras fueran bailes y no cajas de metal y semiconductor? 

el taller está dirigido a quienes les llame la atención esta pregunta, ya sean profesionales de la informática, practicantes de disciplinas de movimiento, artistas, filósofxs o público en general. 

no es necesario tener experiencia previa con actividades basadas en movimiento. además, todo lo que hagamos está diseñado para adaptarse a diferentes grados de movilidad.


# bio
compudanzas es un proyecto creativo de investigación que explora formas alternativas de aprender y hacer cómputo.

tratamos de transicionar desde una lógica de productividad y eficiencia, y circuitos que destruyen vida, hacia danzas, rituales, y otros tipos de computadoras aparentemente inútiles.

nos movemos con calma, paciencia y curiosidad. 

originaries de la ciudad de méxico, ahora residimos en la comunidad de madrid.
