# Here we go: Intro, research and notes - biosejo 120220503

In which I introduce {biosejo} and talk about my plans regarding research, note-taking, and movement.

# Intro

I tend to enjoy writing in chronological posts.

Now that I think of it, I've done that in several instances for a period corresponding to at least half of my life.

Here in the compudanzas site I've been exploring a {wiki}-like structure, because non-linear arrangements, and specifically hypertext-based ones, have attracted me for a long time as well. I hadn't had the chance to explore this until I received plenty of inspiration from the Merveilles webring, especially from the XXIIVV wiki.

=> https://webring.xxiivv.com/ Merveilles webring
=> https://wiki.xxiivv.com/site/home.html XXIIVV

I have enjoyed the process a lot. I even started a digital Zettelkasten now, to create a living archive of my knowledge that I can explore to find new insights (more on this below).

However, I also enjoy reading other people's chronological posts. I was okay reading them, not writing them, until... something (relevant) changed.

I'm starting a predoctoral research position focusing on playful applications of computing to improve proprioception and movement!

We have yet to move to another country in another continent. Meanwhile I've been reading, noticing that this would be a great time to improve my long-term retention of what I read, and therefore deciding to improve how I actually read and learn.

This is where the Zettelkasten comes into play, but I'll talk more about it in a moment.

The thing is that I decided that it would be a great exercise for me to linearize here not only my research, but also the surrounding events (aka "life"). On one hand I love knowledge-sharing, on the other this could be a way to keep close people (that will now be relatively far away) in touch with what I'm living, and on the third hand (or first foot?) I find this could be a great way to practice my writing.

My intention is to write and share here once a week.

# The Zettelkasten

Very simply put, the Zettelkasten is a method to take and organize interconnected notes that tend to represent someone's knowledge. You can read more about it in the following link; these past few days I've been reading many of those posts!

=> https://zettelkasten.de/posts/overview/ Getting Started - Zettelkasten Methon

I decided to start one because I realized that for my preliminary readings for the PhD program I wasn't really taking meaningful notes.

Additionally, because what I'll be doing will be super interdisciplinary: relating movement, psychology, social sciences, philosophy, neuroscience, and also computer science and electronics, I thought that I would need a way to make sense of all this and to avoid getting lost.

I had read about the method already, but now I went deeper and then started with a simple system based on plain text files. The automation and ergonomics will come later whenever I start feeling friction.

My objective here is to improve my reading, being more deliberate about it. I tend to read relatively fast, sometimes just skimming, and I kind of "get" the main points, especially when I talk with my partner about them.

However, I notice I could get a lot more out of reading if I take a more attentive and careful approach, creating and tending notes, interconnecting them with previous ones. Also, I can see how this can lead to longer-term retention and knowledge creationg.

I'm excited about it!

# Movement and less screens

Lastly, I want to share that I'm currently going through a process of simplification. Following a relatively common trend in my circles, I want to spend less time online, and less time in distraction (procrastination disguised as distraction?).

For the zettelkasten I'm taking notes in paper first, and I'm also using paper notebooks to organize my ideas. This very post was outlined in paper!

Also, the text-based focus of what I'm doing right now means that I can use my main computer in "text mode" or "console mode", and that I can use my android tablet with termux and a keyboard to have a similar and more portable writing experience.

=> https://termux.com/ Termux

I'm also getting back to running and moving, and I'm being more deliberate with my on-screen time by using a timer.

It has been great to alternate focus with breaks, in contrast to being aimlessly in front of the computer for a long time!

I'm really excited about all this and what's coming next.

I reiterate my intention: See you next week!
