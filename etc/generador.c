/* generador en proceso */
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#define NPAGES 256

typedef struct{
	char filenames[NPAGES][256]; /* array of filenames */
	char gminames[NPAGES][256];
	char htmlnames[NPAGES][256];
	char wikinames[NPAGES][256];
        int incominglinks[NPAGES][NPAGES]; /* array of indexes */
	int nilinks[NPAGES]; /* how many incoming links per page */
	int count; /* total number of pages */
} Pages;

void fputgmilink(FILE *f, Pages * p, int index){
	fprintf(f,"=> ./%s %s\n",p->gminames[index],p->wikinames[index]);
}

int main(int argc, char * argv[]){
	DIR * d;
	FILE * f, *fgem, *fweb;
	struct dirent * entry;
	Pages p;
	int i,j,k;
	for(i=0; i<NPAGES; i++){
		p.nilinks[i] = 0;
	}

	/* clean pages.gmo */
	remove("src/pages.gmo");

	/* open directory */
	if( !(d = opendir("src")) ){
		fprintf(stderr,"failed to open directory");
		return 1;
	}

	int index=0;
        char * ret;
	/* for each file */
	while( (entry = readdir(d)) ){

		/* find gmo files */
		if( (ret = strstr( entry->d_name, ".gmo") ) ){
			/* add to index */
			strcpy( p.filenames[index], "src/");
			strcat( p.filenames[index], entry->d_name );

			/* add gmi name */
			strcpy( p.gminames[index], entry->d_name );
			ret = strstr( p.gminames[index], ".gmo");
			strcpy( ret, ".gmi");

			/* add html name */
			strcpy( p.htmlnames[index], entry->d_name );
			ret = strstr( p.htmlnames[index], ".gmo");
			strcpy( ret, ".html");

			/* add wikiname */
			strcpy( p.wikinames[index], entry->d_name );
			ret = strstr(p.wikinames[index], ".gmo");
			*ret = '\0'; /* remove suffix */
			/* convert _ to spaces: */
			for(i=0; p.wikinames[index][i]; i++){
				if(p.wikinames[index][i]=='_')
					p.wikinames[index][i] = ' ';
			}
			index++;
		}
	}
	p.count = index;
	printf("%d gmo files\n",p.count);
	closedir(d); /* close directory */

	/* write index: pages.gmo */
	f = fopen("src/pages.gmo","w");
	fputs("# index of pages\n\n",f);
	for(i=0; i<p.count; i++){
		fputgmilink(f,&p,i);
	}
	fclose(f);

	/* search for incoming links */
	int premode = 0;
	char line[1024],newline[1024];
	char link[1024];
	int found = 0;
	for(i=0; i<p.count; i++){
		premode = 0;
		printf("scanning %s for outgoing links...\n",p.filenames[i]);
		f = fopen(p.filenames[i],"r");
		while( fgets(line, 1024, f) ){
			if( strncmp( line, "```", 3) == 0){ /* check for pre-mode */
				premode = !premode;
			}
			if( !premode ){
				/* search for wikilink */
				if( (ret=strchr(line,'{') )){
					for(j=1; ret[j]!='}';j++){
						link[j-1]=ret[j];
					}
					link[j-1] = '\0'; /* link has wikilink name now */
					/* search for wikiname to assign incoming link: */
					for(j=0; j<p.count; j++){
						if(strcmp(p.wikinames[j],link)==0){
							/* j is wikiname index */
							found = 0;
							for(k=0;k<p.nilinks[j] && found<1;k++){
								if(p.incominglinks[j][k]==i)
									found = 1;
							}
							if(!found){
								p.incominglinks[j][ p.nilinks[j] ] = i;
								p.nilinks[j]++;
							}
						}
					}
					printf("%s\n",link);
				}
			}
		}
		fclose(f);
	}

	printf("starting conversion...\n");

	/* start conversion */
	char gemfilename[1024], webfilename[1024];
	for(i=0; i<p.count; i++){
		printf("%s has %d incoming links\n",p.wikinames[i],p.nilinks[i]);
		f = fopen(p.filenames[i],"r");
		strcpy(gemfilename,"gemtest/");
		strcat(gemfilename,p.gminames[i]);
		fgem = fopen(gemfilename,"w");
		premode = 0;
		while( fgets(line, 1024, f) ){
			if( strncmp( line, "```", 3) == 0){
				premode = !premode;
			}
			/* for gemini */
			/* TODO: remove { } from body */
			if(strncmp( line, "+", 1) != 0 ){ /* skip '+' lines */
				if(strncmp(line,"& ",2) == 0){ /* remove & prefix */
					fputs(&line[2], fgem);
				}
				else if( !premode && strncmp(line,"=>",2)!=0 && (ret=strchr(line,'{')) ){
					for(j=1; ret[j]!='}'; j++){
						link[j-1] = ret[j];
					}
					link[j-1] = '\0';
					index = -1;
					for(j=0; j<p.count && index<0; j++){
						if(strcmp(p.wikinames[j],link)==0)
							index = j;
					}
					fputs(line,fgem);
					fputgmilink(fgem,&p,index);
				}
				else{
					fputs(line, fgem);
				}
			}
		}
		fprintf(fgem, "\n# incoming links\n");
		for(j=0; j<p.nilinks[i]; j++){
			fputgmilink(fgem,&p,j);
		}
		fclose(fgem);
		fclose(f);
	}


	return 0;
}
